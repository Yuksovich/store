package com.kozorezov.controller;

import com.kozorezov.domain.Product;
import com.kozorezov.sevice.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public final class ApiController {
    private final StoreService storeService;

    @Autowired
    public ApiController(final StoreService storeService) {
        this.storeService = storeService;
    }

    @RequestMapping(
            path = "/category",
            method = RequestMethod.GET,
            produces = {"application/json; charset=UTF-8"})
    public List getCategories() {
        return storeService.getCategories();
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/{category}",
            produces = {"application/json; charset=UTF-8"})
    public List getProductsByCategory(@PathVariable("category") final String category) {
        return storeService.getProductsByCategory(category);
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/product/{id}",
            produces = {"application/json; charset=UTF-8"})
    public Product getProductById(@PathVariable("id") final long id) {
        return storeService.getProductById(id);
    }
}
