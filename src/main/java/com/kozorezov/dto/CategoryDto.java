package com.kozorezov.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CategoryDto {
    private String name;
    private List<CategoryDto> subCategories;

    @JsonProperty("text")
    public String getText() {
        return name;
    }

    @JsonProperty("name")
    public void setName(final String name) {
        this.name = name;
    }

    @JsonProperty("nodes")
    public List<CategoryDto> getNodes() {
        return subCategories;
    }

    @JsonProperty("subCategories")
    public void setSubCategories(final List<CategoryDto> subCategories) {
        this.subCategories = subCategories;
    }

    @JsonProperty("selectable")
    public boolean isSelectable(){
        return false;
    }
}
