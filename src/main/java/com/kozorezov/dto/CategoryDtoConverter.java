package com.kozorezov.dto;

import com.kozorezov.domain.Category;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class CategoryDtoConverter implements Converter<Category, CategoryDto> {
    @Override
    public CategoryDto convert(final Category source) {
        if (source == null) {
            return null;
        }
        final CategoryDto dto = new CategoryDto();
        dto.setName(source.getName());
        dto.setSubCategories(
                source.getSubCategories()
                        .stream()
                        .map(this::convert)
                        .collect(Collectors.toList()));
        return dto;
    }
}
