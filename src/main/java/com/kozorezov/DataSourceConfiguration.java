package com.kozorezov;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfiguration {
    @Value(value = "${jdbc.driverClassName}")
    private String dbDriver;

    @Value(value = "${jdbc.url}")
    private String dbUrl;

    @Value(value = "${jdbc.username}")
    private String dbUsername;

    @Value(value = "${jdbc.password}")
    private String dbPassword;

    @Bean
    @Qualifier(value = "mDataSource")
    public DataSource getDataSource(){
        final BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(dbDriver);
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(dbUsername);
        dataSource.setPassword(dbPassword);
        return dataSource;
    }
}
