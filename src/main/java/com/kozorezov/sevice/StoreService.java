package com.kozorezov.sevice;

import com.kozorezov.domain.Product;

import java.util.List;

public interface StoreService {
    List getCategories();

    List getProductsByCategory(String category);

    Product getProductById(long id);
}
