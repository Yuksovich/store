package com.kozorezov.sevice;

import com.kozorezov.dto.CategoryDto;
import com.kozorezov.domain.Category;
import com.kozorezov.domain.Product;
import com.kozorezov.repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StoreServiceImpl implements StoreService {
    private final StoreRepository storeRepository;
    private final Converter<Category, CategoryDto> converter;

    @Autowired
    public StoreServiceImpl(final StoreRepository storeRepository,
                            final Converter<Category, CategoryDto> converter) {
        this.storeRepository = storeRepository;
        this.converter = converter;
    }

    @Override
    @Transactional(readOnly = true)
    public List getCategories() {
        return storeRepository.getAllCategories().stream()
                .map(converter::convert).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List getProductsByCategory(final String category) {
        return storeRepository.getAllProductsByCategory(category).stream()
                .peek(p-> p.setCategoryName(p.getCategory().getName()))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Product getProductById(final long id) {
        return storeRepository.getProductById(id);
    }
}
