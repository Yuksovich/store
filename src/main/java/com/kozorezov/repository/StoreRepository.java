package com.kozorezov.repository;

import com.kozorezov.domain.Category;
import com.kozorezov.domain.Product;

import java.util.List;

public interface StoreRepository {
    List<Category> getAllCategories();

    List<Product> getAllProductsByCategory(String category);

    Product getProductById(long id);
}
