package com.kozorezov.repository;

import com.kozorezov.domain.Category;
import com.kozorezov.domain.Product;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class StoreRepositoryImpl implements StoreRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public StoreRepositoryImpl(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Category> getAllCategories() {
        @SuppressWarnings("unchecked") final List<Category> categoryList =
                (List<Category>) sessionFactory.getCurrentSession()
                .createQuery("select distinct c from Category c left join fetch c.subCategories")
                .list();
        return categoryList.stream().filter(c -> c.getParentCategory() == null).collect(Collectors.toList());
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Product> getAllProductsByCategory(final String categoryName) {
        final Category category = new Category(categoryName);

        return (List<Product>) sessionFactory.getCurrentSession()
                .createCriteria(Product.class)
                .add(Restrictions.eq("category", category))
                .list();
    }

    @Override
    public Product getProductById(final long id) {
        final Product product = sessionFactory.getCurrentSession().get(Product.class, id);
        product.setCategoryName(product.getCategory().getName());
        return product;
    }
}
