package com.kozorezov.domain.disc;

public enum DiscContent {
    MUSIC, VIDEO, SOFTWARE;
}
