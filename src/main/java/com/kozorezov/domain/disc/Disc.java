package com.kozorezov.domain.disc;

import com.kozorezov.domain.Product;

import javax.persistence.*;

@Entity
@DiscriminatorValue(value = "DISC")
public class Disc extends Product {

    @Column(name = "TYPE")
    private DiscType discType;

    @Column(name = "CONTENT")
    private DiscContent discContent;

    @Enumerated(EnumType.ORDINAL)
    public DiscType getDiscType() {
        return discType;
    }

    public void setDiscType(final DiscType discType) {
        this.discType = discType;
    }

    @Enumerated(EnumType.ORDINAL)
    public DiscContent getDiscContent() {
        return discContent;
    }

    public void setDiscContent(final DiscContent discContent) {
        this.discContent = discContent;
    }
}
