package com.kozorezov.domain.disc;

public enum DiscType {
    CD, DVD;
}
