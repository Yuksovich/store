package com.kozorezov.domain.book;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "ESOTERIC_BOOK")
public class EsotericBook extends Book {

    @Column(name = "MIN_AGE")
    private int minAge;

    public int getMinAge() {
        return minAge;
    }

    public void setMinAge(final int minAge) {
        this.minAge = minAge;
    }
}
