package com.kozorezov.domain.book;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "PROGRAMMING_BOOK")
public class ProgrammingBook extends Book {

    @Column(name = "PROGRAMMING_LANGUAGE", length = 20)
    private String programmingLanguage;

    public String getProgrammingLanguage() {
        return programmingLanguage;
    }

    public void setProgrammingLanguage(final String programmingLanguage) {
        this.programmingLanguage = programmingLanguage;
    }
}
