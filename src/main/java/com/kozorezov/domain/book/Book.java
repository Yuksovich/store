package com.kozorezov.domain.book;

import com.kozorezov.domain.Product;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public abstract class Book extends Product {

    @Column(name = "NUMBER_OF_PAGES")
    private int numberOfPages;

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(final int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }
}
