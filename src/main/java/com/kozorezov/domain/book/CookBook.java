package com.kozorezov.domain.book;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "COOK_BOOK")
public class CookBook extends Book {

    @Column(name = "MAIN_INGREDIENT", length = 20)
    private String mainIngredient;

    public String getMainIngredient() {
        return mainIngredient;
    }

    public void setMainIngredient(final String mainIngredient) {
        this.mainIngredient = mainIngredient;
    }
}
