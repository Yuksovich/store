package com.kozorezov.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "CATEGORY")
public class Category {
    @Id
    @Column(name = "NAME", length = 50)
    private String name;

    @OneToMany(targetEntity = Product.class, mappedBy = "category")
    private List<Product> products;

    @ManyToOne(targetEntity = Category.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_CATEGORY_NAME")
    private Category parentCategory;

    @OneToMany(targetEntity = Category.class, mappedBy = "parentCategory")
    private List<Category> subCategories;

    public Category() {
    }

    public Category(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(final List<Product> products) {
        this.products = products;
    }

    public Category getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(final Category parentCategory) {
        this.parentCategory = parentCategory;
    }

    public List<Category> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(final List<Category> subCategories) {
        this.subCategories = subCategories;
    }
}
