INSERT INTO CATEGORY VALUES
  ('Products', NULL ),
  ('Books', 'Products'),
  ('Discs', 'Products'),
  ('Programming Books', 'Books'),
  ('Cook Books', 'Books'),
  ('Esoteric Books', 'Books');

INSERT INTO
  PRODUCT (BARCODE, NAME, PRICE, NUMBER_OF_PAGES, MAIN_INGREDIENT, PRODUCT_TYPE, CATEGORY_NAME)
VALUES
  ('0123-4567', 'Nourishing Traditions', 25.95, 320, 'Meat', 'COOK_BOOK', 'Cook Books'),
  ('1243-4542', 'The Edible Garden', 15.95, 150, 'Vegetables', 'COOK_BOOK', 'Cook Books'),
  ('7689-2121', 'Fix-It and Forget-It Cooking', 32.95, 230, 'Fish', 'COOK_BOOK', 'Cook Books');

INSERT INTO
  PRODUCT (BARCODE, NAME, PRICE, NUMBER_OF_PAGES, PROGRAMMING_LANGUAGE, PRODUCT_TYPE, CATEGORY_NAME)
VALUES
  ('3457-1346', 'Effective Java', 19.95, 369, 'Java', 'PROGRAMMING_BOOK', 'Programming Books'),
  ('0587-2576', 'C# 6.0 and the .NET 4.6 Framework', 41.95, 1660, 'C#', 'PROGRAMMING_BOOK', 'Programming Books'),
  ('9565-1435', 'Professional JavaScript for Web Developers', 37.95, 960, 'JavaScript', 'PROGRAMMING_BOOK',
   'Programming Books');

INSERT INTO
  PRODUCT (BARCODE, NAME, PRICE, NUMBER_OF_PAGES, MIN_AGE, PRODUCT_TYPE, CATEGORY_NAME)
VALUES
  ('2632-7457', 'The Secret Teachings of All Ages', 21.95, 342, 16, 'ESOTERIC_BOOK', 'Esoteric Books'),
  ('1241-2355', 'The Celestine Prophecy', 32.95, 235, 18, 'ESOTERIC_BOOK', 'Esoteric Books'),
  ('3464-1512', 'The Art of Dreaming', 29.95, 310, 12, 'ESOTERIC_BOOK', 'Esoteric Books');

INSERT INTO
  PRODUCT (BARCODE, NAME, PRICE, CONTENT, TYPE, PRODUCT_TYPE, CATEGORY_NAME)
VALUES
  ('1245-2351', 'AC-DC', 8.95, 0, 0, 'DISC', 'Discs'),
  ('0986-6942', 'Star Wars', 12.95, 1, 1, 'DISC', 'Discs'),
  ('2390-0982', 'Windows XP', 49.95, 2, 0, 'DISC', 'Discs');



