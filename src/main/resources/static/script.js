$(document).ready(function () {
    $.getJSON('/api/category', function (data) {
        createTree(data);
    });
});

var productsCache = [];

function createTree(data) {
    var openedNodeId;
    $('#tree').treeview({
        data: data,
        levels: 2,
        enableLinks: false,
        onNodeExpanded: function (event, node) {
            if (node.nodes.length === 0) {
                collapseNode(openedNodeId);
                openedNodeId = node.nodeId;
                removeProductsFromContent();
                $.getJSON('/api/' + node.text, function (products) {
                    productsCache = [];
                    for (var i = 0; i < products.length; i++) {
                        var product = products[i];
                        productsCache[product.id] = product;
                        $('#content').append(
                            '<div class="list-group-item product" id="' + product.id + '">' +
                            '<a href="/">' + product.name + '</a></div>');
                    }
                    setClickListener();
                });
            }
        }
    });

    function setClickListener() {
        $('.product').click(function (event) {
            event.preventDefault();
            collapseNode(openedNodeId);
            removeProductsFromContent();
            createProductDescription(productsCache[$(this).attr('id')]);
        })
    }
}

function removeProductsFromContent() {
    $('.product').remove();
}

function collapseNode(nodeId) {
    if (nodeId) {
        $('#tree').treeview('collapseNode', [nodeId, {silent: true, ignoreChildren: false}]);
    }
}

function createProductDescription(product) {
    var desc =
        '<div class="product">' +
        '<div>' +
        '<h2>' + product.name + '</h2>' + product.categoryName +
        '</div>' +
        '<div style="float: right"><h3>Price: $' + product.price + '</h3></div>' +
        '<div><h4>' + product.barcode + '</h4></div>' +
        '<div class="col-lg-3 description"></div>' +
        '</div>';
    $('#content').append(desc);
    if (product.numberOfPages) {
        addDescription("Number of pages: ", product.numberOfPages);
    }
    if (product.mainIngredient) {
        addDescription("Main ingridient: ", product.mainIngredient);
    }
    if (product.discContent) {
        addDescription("Content Type: ", product.discContent);
    }
    if (product.discType) {
        addDescription("Disc type: ", product.discType);
    }
    if (product.minAge) {
        addDescription("Minimum age: ", product.minAge);
    }
    if (product.programmingLanguage) {
        addDescription("Programming language: ", product.programmingLanguage);
    }
}

function addDescription(tag, description) {
    $('.description').append('<div class="container"><h4><b>' + tag + "</b> " + description + '</h4></div>');
}
